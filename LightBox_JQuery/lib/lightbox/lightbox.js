var gallery = (function(){
	return {
		modules: {}
	}
})();

gallery.modules.lightbox = (function() {
	var img;
    var slided = false;
    var image;

	return {


		afficher : function(){

		    image = $(this).find('img').attr("data-img");
			var title = $(this).find('img').attr("title");
			img = $(this);
            active = $(this);
        	$('.img').attr('src', image);
        	$('.titre').text(title);
        	$('.modal').fadeIn(500);


            if (localStorage.getItem(image) == ''){
                $('.comContent').empty();
            }else{
                $('.comContent').empty();
                var com = localStorage.getItem(image);
                $('.comContent').append(com);
            }

		},

		supprimer : function(){
        	$('.modal').fadeOut(500);
        	
        },

        next : function(){


            if($(img).next().is('div')){
                console.log("ok");
                var img2 =img.next();
                image = img.next().find('img').attr("data-img");
                var title = img.next().find('img').attr("title");
                img = $(img2);
                $('.img').attr('src', image);
                $('.titre').text(title);
            }
            else
            {
                console.log("Pas ok");
                var img2 = $(img).parent().children('div').first();
                image = $(img).parent().children('div').first().find('img').attr('data-img');
                var title = $(img).parent().children('div').first().find('img').attr("title");
                img = $(img2);
                $('.img').attr('src', image);
                $('.titre').text(title);
            }


            if (localStorage.getItem(image) == ''){
                $('.comContent').empty();
            }else{
                $('.comContent').empty();
                var com = localStorage.getItem(image);
                $('.comContent').append(com);
            }


        },

        previous : function(){

        	if($(img).prev().is('div')){
                console.log("ok");
                var img2 =img.prev();
                image = img.prev().find('img').attr("data-img");
                var title = img.prev().find('img').attr("title");
                img = $(img2);
                $('.img').attr('src', image);
                $('.titre').text(title);
            }
            else
            {
                console.log("Pas ok");
                var img2 = $(img).parent().children('div').last();
                image = $(img).parent().children('div').last().find('img').attr('data-img');
                var title = $(img).parent().children('div').last().find('img').attr("title");
                img = $(img2);
                $('.img').attr('src', image);
                $('.titre').text(title);
            }


            if (localStorage.getItem(image) == ''){
                $('.comContent').empty();
            }else{
                $('.comContent').empty();
                var com = localStorage.getItem(image);
                $('.comContent').append(com);
            }

        },

        slide : function(){

            if(!this.slided){
                $('.slidingContent').slideDown('600');
                this.slided = true;
            }else{
                $('.slidingContent').slideUp('600');
                this.slided = false;
            }
        },

        comment : function(){

            if($('#comment').val() == ''){
                
            }
            else{
                console.log('oui');
                var com = $('.comContent').html();
                var com2 = '<br>' + $('#comment').val();
                var commentaire = com + '</br>' + $('#comment').val();
                localStorage.setItem(image, commentaire);
                $('.comContent').append(com2);
                $('#comment').empty();

            }
        },

		init : function() {

			$('.vignette').click(gallery.modules.lightbox.afficher);
			$('#modal_close').stop().click(gallery.modules.lightbox.supprimer);
			$('#modal_next').click(gallery.modules.lightbox.next);
			$('#modal_previous').click(gallery.modules.lightbox.previous);
            $('#showContent').click(gallery.modules.lightbox.slide);
            $('#addComment').click(gallery.modules.lightbox.comment);


            //localStorage.clear();

            $(document).keyup(function(k){
                if(k.keyCode == "37") gallery.modules.lightbox.previous();

                else if (k.keyCode == "39") gallery.modules.lightbox.next();

                else if (k.keyCode == "27") gallery.modules.lightbox.supprimer();
            });
		}

	}
})();


$(document).ready( function(){
	$('body').append($('<div class="modal" id="modal1"><div id="modal_content"><div id="slider"><div class="slidingContent"><div class="comContent"></div><input type="text" id="comment"></input><button id="addComment">Ajouter</button></div><button id="showContent">Commentaires</button></div><div class="titre"></div><img class="img" src=""><button id="modal_close">X</button><button id="modal_next">></button><button id="modal_previous"><</button></div></div>'));
	gallery.modules.lightbox.init();
});