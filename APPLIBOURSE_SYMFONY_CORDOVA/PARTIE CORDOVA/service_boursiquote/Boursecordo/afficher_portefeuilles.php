<?php

include "vendor/autoload.php";

include "./models/PdoPortefeuilles.php";
require_once("./TechnicalClasses/TradingUtils.php");

$pdo = new PdoPortefeuilles();
$res = $pdo->getPortefeuillesUser($_GET['log']);

for($i=0; $i<count($res); $i++){
    $res[$i] = TradingUtils::situationCourante($res[$i]);
}

// une fois que j'ai les portefeuilles, il faut que je mette � jour ses informations � partir des derniers cours
//echo var_dump($res[0]);


//echo var_dump($res[0]);
if ($res){
    echo json_encode($res);
}else{
    echo json_encode(false);
}
?>