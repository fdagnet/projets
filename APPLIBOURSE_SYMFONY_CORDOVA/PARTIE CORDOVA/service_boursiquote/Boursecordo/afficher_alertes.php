<?php

include "vendor/autoload.php";

include "./models/PdoAlertes.php";
require_once("./TechnicalClasses/TradingDetails.php");

$pdo = new PdoAlertes();

$res = $pdo->getAlertes($_GET['log']);

for($i=0; $i<count($res); $i++){
    $res[$i] = TradingDetails::InfoAlerte($res[$i]);}

if ($res==true){
    echo json_encode($res);
}else{
    echo json_encode(false);
}