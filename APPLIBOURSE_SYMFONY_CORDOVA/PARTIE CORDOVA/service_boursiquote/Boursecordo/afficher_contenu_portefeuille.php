<?php

include "vendor/autoload.php";

include "./models/PdoPortefeuilles.php";
require_once("./TechnicalClasses/TradingDetails.php");

$pdo = new PdoPortefeuilles();

$res = $pdo->getContenuPortefeuille($_GET['numport']);

for($i=0; $i<count($res); $i++){
    $res[$i] = TradingDetails::situationCourante($res[$i]);
}

if ($res){
    echo json_encode($res);
}else{
    echo json_encode(false);
}
?>