<?php

require_once("models/PdoPortefeuilles.php");

use DirkOlbrich\YahooFinanceQuery\YahooFinanceQuery;

/**
 * Created by PhpStorm.
 * User: fdagnet
 * Date: 20/04/2016
 * Time: 11:19
 */
class TradingUtils
{
    public static function situationCourante($unP){
        $pdo = new PdoPortefeuilles();
        $res = $pdo->getContenuPortefeuille($unP['id']);

        $valeurVeille = 0;
        $valeurCourante = 0;

        for($i = 0; $i < count($res); $i++){
            $actionCourante = $res[$i];

            //echo "ANALYSE DE LA LIGNE POUR LA VALEUR " .$actionCourante['ticker']."<br/>";

            // je dois r�cup�rer la valeur des infos sur Yahoo
            $query = new YahooFinanceQuery;
            $symbol = array();
            $symbol[] = $actionCourante['ticker'];
            $data = $query->quote($symbol)->get();


            $veille = $data[0]['PreviousClose'];
            $courant = $data[0]['LastTradePriceOnly'];
            $variation = $data[0]['ChangeinPercent'];

            // on r�cup�re combien on a d'actions de la valzeur courante
            $nombre = $actionCourante['nombre'];


            $valeurVeille += ($veille * $nombre);
            $valeurCourante += ($courant * $nombre);

            /*
            echo "Veille : " .$veille;
            echo "Dernier : " .$courant;
            echo "Variation : " .$variation;
            */
        }


        $prix_totalAvant = $pdo->getPerfPort($unP);
        $prix_total = ((($courant * $nombre)-($prix_totalAvant[0]))/$prix_totalAvant[0])*100;
        $unP['variation_initiale'] = round($prix_total,2);


        $variationglobale = round((($valeurCourante/$valeurVeille)-1)*100,3);

        $unP['variation'] = $variationglobale;
        $unP['valoGlobale'] = $valeurCourante + $unP['montant'];
        return $unP;
    }

}