<?php
require_once("models/PdoPortefeuilles.php");


use DirkOlbrich\YahooFinanceQuery\YahooFinanceQuery;
/**
 * Created by PhpStorm.
 * User: fdagnet
 * Date: 20/04/2016
 * Time: 15:58
 */
class TradingDetails
{
    public static function situationCourante($unP){
        $pdo = new PdoPortefeuilles();
//echo var_dump($unP);
        for($i = 0; $i < count($unP['ticker']); $i++){

            //echo "ANALYSE DE LA LIGNE POUR LA VALEUR " .$unP['ticker']."<br/>";

            // je dois r�cup�rer la valeur des infos sur Yahoo
            $query = new YahooFinanceQuery;
            $symbol = array();
            $symbol[] = $unP['ticker'];
            $data = $query->quote($symbol)->get();


            $courant = $data[0]['LastTradePriceOnly'];
            $variation = $data[0]['ChangeinPercent'];

            // on r�cup�re combien on a d'actions de la valzeur courante
            $nombre = $unP['nombre'];



            /*
            echo "Veille : " .$veille;
            echo "Dernier : " .$courant;
            echo "Variation : " .$variation;
            */
        }
        $prix_totalAvant = $pdo->getPerf($unP);
        $prix_total = ((($courant * $nombre)-($prix_totalAvant[0]))/$prix_totalAvant[0])*100;
        $unP['variation_initiale'] = round($prix_total,2);
        $montant = $courant * $nombre;
        $unP['montant'] = $montant;
        $unP['cours'] = $courant;
        $unP['variation'] = $variation;
        return $unP;
    }

    public static function InfoAlerte($uneA){
        $pdo = new PdoAlertes();

        for($i = 0; $i < count($uneA['ticker']); $i++){

            $query = new YahooFinanceQuery;
            $symbol = array();
            $symbol[] = $uneA['ticker'];
            $data = $query->quote($symbol)->get();

            $courant = $data[0]['LastTradePriceOnly'];
            $nom = $data[0]['Name'];
        }
        $uneA['cours'] = $courant;
        $uneA['libelle'] = $nom;
        return $uneA;

    }
}