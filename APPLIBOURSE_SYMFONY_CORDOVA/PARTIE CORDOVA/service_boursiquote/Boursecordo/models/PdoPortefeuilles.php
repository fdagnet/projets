<?php

require_once("PdoBoursoBase.php");

class PdoPortefeuilles extends PdoBoursoBase {

    public function __construct(){
        parent::__construct();
    }
    /**
     * Retourne toutes les actions sous forme d'un tableau associatif
     *
     * @return le tableau associatif des actions
     */

    public function getPortefeuillesUser($log)
    {
        $req = "select NumPort as id,Nom as libelle,Solde as montant from portefeuille where proprietaire= '$log'";
        $res = $this->monPdo->query($req);
        $lesLignes = $res->fetchAll();
        if(count($lesLignes) == 0)
        {
            return false;
        }
        else
        {
            return $lesLignes;
        }
    }

    public function getContenuPortefeuille($numport){
        $req ="select appartenir.NumPort as numport, appartenir.Ticker as ticker,appartenir.Quantite as nombre,Company as libelle from appartenir INNER JOIN actions ON appartenir.Ticker=actions.Ticker INNER JOIN portefeuille ON appartenir.NumPort=portefeuille.NumPort where appartenir.NumPort='$numport'";
        $res = $this->monPdo->query($req);
        $lesLignes = $res->fetchAll();
        if(count($lesLignes) == 0)
        {
            return false;
        }
        else
        {
            return $lesLignes;
        }
    }

    public function getPerf($unP)
    {

        $req ="select (sum(quantite*prix)) as prix_total from operation where Ticker='".$unP['ticker']."' and NumPort=".$unP['numport']."";
        $res = $this->monPdo->query($req);
        $leslignes = $res->fetchAll();
        if(count($leslignes) == 0)
        {
            return false;
        }
        else
        {
            return $leslignes[0];
        }
    }

    public function getPerfPort($unP)
    {
        $req ="select (sum(quantite*prix)) as prix_total from operation where NumPort=".$unP['id']."";
        $res = $this->monPdo->query($req);
        $leslignes = $res->fetchAll();
        if(count($leslignes) == 0)
        {
            return false;
        }
        else
        {
            return $leslignes[0];
        }
    }

}