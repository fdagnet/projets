<?php

require_once("PdoBoursoBase.php");

class PdoAlertes extends PdoBoursoBase {

    public function __construct(){
        parent::__construct();
    }
    /**
     * Retourne toutes les actions sous forme d'un tableau associatif
     *
     * @return le tableau associatif des actions
     */

    public function getAlertes($log)
    {
        $req = "select Ticker as ticker, SeuilBas, SeuilHaut from alerter where Mail='$log'";
        $res = $this->monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
}