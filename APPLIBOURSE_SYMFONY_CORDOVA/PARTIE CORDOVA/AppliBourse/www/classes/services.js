var ServiceAuthentification = function () {

    var url;

    this.initialize = function (serviceURL) {
        url = serviceURL ? serviceURL : "http://localhost/service_boursiquote/Boursecordo/";
    }

    this.authentifier = function (log, mdp) {
        return $.ajax({
            url: url + "authentifier_user.php",
            type: "GET",
            dataType: "json",
            data: {
                log : log,
                mdp : mdp
            }
        });
    }
}

var ServiceGetPortefeuilles = function () {

    var url;

    this.initialize = function (serviceURL) {
        url = serviceURL ? serviceURL : "http://localhost/service_boursiquote/Boursecordo/";
    }

    this.getPortefeuillesUser = function (log) {
        return $.ajax({
            url: url + "/afficher_portefeuilles.php",
            type: "GET",
            dataType: "json",
            data: {
                log : log
            }
        });

        /*var service = new ClassePortefeuilles();
        return service.portefeuilles;*/
    }
}


var ServiceGetContenuPortefeuilles = function () {

    var url;

    this.initialize = function (serviceURL) {
        url = serviceURL ? serviceURL : "http://localhost/service_boursiquote/Boursecordo/";
    }

    this.getPortefeuillesDetail = function (numport) {


        return $.ajax({
            url: url + "/afficher_contenu_portefeuille.php",
            type: "GET",
            dataType: "json",
            data: {
                numport: numport
            }
        });

       /* var service = new ClassePortefeuilles();
        return service.actions;*/
    }
}

var ServiceGetAlertes = function () {

    var url;

    this.initialize = function (serviceURL) {
        url = serviceURL ? serviceURL : "http://localhost/service_boursiquote/Boursecordo/";
    }

    this.getAlertes = function (log) {


        return $.ajax({
            url: url + "/afficher_alertes.php",
            type: "GET",
            dataType: "json",
            data: {
                log: log
            }

        });

        var service = new ClassePortefeuilles();
        return service.alertes;
    }
}