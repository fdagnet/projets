var loginTpl = Handlebars.compile($("#login-tpl").html());
var homeTpl = Handlebars.compile($("#home-tpl").html());
var detailPortTpl = Handlebars.compile($("#detail-port-tpl").html());
var alertesTpl = Handlebars.compile($("#alertes-tpl").html());
var userTpl = Handlebars.compile($("#profile-tpl").html());


var LoginView = function(){
   $('body').html(loginTpl());
   if (window.localStorage.getItem("utilisateur")){
      $('.login').val(window.localStorage.getItem("utilisateur"));
   }
};

var HomeView = function(){

   // on doit récupérer les portefeuilles de l'utilisateur
   var service = new ServiceGetPortefeuilles();
   service.initialize();

   service.getPortefeuillesUser(window.localStorage.getItem("utilisateur")).done(function(data){

      var context = { listePortefs : data };

      $('body').html(homeTpl(context));
   });

};

var DetailPortView = function(numport){
   // on doit récupérer les portefeuilles de l'utilisateur
   var service = new ServiceGetContenuPortefeuilles();
   service.initialize();

   service.getPortefeuillesDetail(numport).done(function(data){
      var context = { nomPortefeuille:'mon premier portefeuille', listeActions : data };

      $('body').html(detailPortTpl(context));
   });




};

var AlertesListView = function(log){
   // on doit récupérer les portefeuilles de l'utilisateur
   var service = new ServiceGetAlertes();
   service.initialize();
   var permanentStorage = window.localStorage;

   var loginco = permanentStorage.getItem("utilisateur");
   service.getAlertes(loginco).done(function(data){
      var context = { listeAlertes : data };
      $('body').html(alertesTpl(context));
   });


};

var UserView = function(){
   $('body').html(userTpl());

   var permanentStorage = window.localStorage;

   var loginco = permanentStorage.getItem("utilisateur");
   $('.logco').html(loginco);

};