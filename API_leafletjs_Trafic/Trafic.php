<!-- WEBETU : https://webetu.iutnc.univ-lorraine.fr/www/dagnet2u/Interop%C3%A9rabilit%C3%A9_Trafic/Trafic.php -->

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
		<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
	</head>
	<body>

	<?php

	$url = 'http://api.loire-atlantique.fr:80/opendata/1.0/traficevents?filter=Tous';

	//$opts = array('http' => array('proxy' => 'tcp://www-cache:3128', 'request_fulluri'=> true));
	$opts = array('http' => array('proxy'=> 'tcp://127.0.0.1', 'request_fulluri'=> true));
	
	$context = stream_context_create($opts);

	$json_content = file_get_contents($url/*, FILE_USE_INCLUDE_PATH, $context*/);

	$location = json_decode($json_content);

	?>

	<br><br><center><h1>InfoTrafic Nantes</h1></center>
	<center><div id="mapid" style="height:600px; width:900px;"></div></center>
</body>
</html>
<script type="text/javascript">
	var mymap = L.map('mapid').setView([47.216671,-1.55],9);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
	maxZoom: 18,
	id: 'mapbox.streets'
	}).addTo(mymap);

	mymap.on('click', onMapClick);

</script>

<?php 


	foreach ($location as $loc){
		 $longitude = $loc->longitude;
		 $latitude = $loc->latitude;
		 $ligne = $loc->ligne1;
		 $ligne2 = $loc->ligne4;
		 $ligne3 = $loc->ligne2;

		echo "<script>";
		echo "L.marker([{$latitude}, {$longitude}]).addTo(mymap).bindPopup(\"{$ligne}<br>{$ligne3}<br>{$ligne2}\");";
		echo "</script>";
		//var marker = L.marker([$data->latitude, $data->longitude]).addTo(mymap):
		

	}

	

	//var_dump($location);

?>

