angular.module('tache').controller('TachesController',
	['$scope','$http','$rootScope',

	function($scope,$http,$rootScope){
		$scope.taches =[];
		$scope.taches = $rootScope.listetaches;

		$scope.addTache=function(){
			console.log($scope.nameTache);
			if($scope.nameTache==''){
			   alert('Impossible d\'ajouter une tâche sans nom !! ');
			}else{
				 $http.post('http://todos.api.netlor.fr/lists/'+$rootScope.selectedlist+"/todos", { text: $scope.nameTache })
				.then(function(response){
						$http.get('http://todos.api.netlor.fr/lists/'+$rootScope.selectedlist+'/todos')
						.then(function(response){
						$rootScope.listetaches =[];
						response.data.forEach(function(data){
						$rootScope.listetaches.push(data);
					});
				});
				},function(error){

				})	;
				
			}
		},

		$scope.tacheCheck=function(tache){
			
			if(tache.done==true){ 
				$http.put('http://todos.api.netlor.fr/lists/'+tache.list_id["$oid"]+'/todos/'+tache.id+"/done")
				.then(function(response){
					console.log(tache.done);
				},function(error){

				});
			}else{
				$http.put('http://todos.api.netlor.fr/lists/'+tache.list_id["$oid"]+'/todos/'+tache.id+"/undone")
				.then(function(response){
					console.log(tache.done);
				},function(error){

				});
			}
		},

		$scope.remove= function(tacheId){
			 $rootScope.listetaches.forEach(function(t){
			 	if(t.id==tacheId){
			 		$rootScope.listetaches.splice($rootScope.listetaches.indexOf(t), 1);
			 	}
			});
		},


		$scope.deleteTache=function(tache){
		$http.delete('http://todos.api.netlor.fr/lists/'+tache.list_id["$oid"]+'/todos/'+tache.id)
		.then(function(response){

			$scope.remove(tache.id);
			 
		
		});
		
	},


	$scope.updateTache=function(tache){
		if(!tache.changeNameTache==''){
			$http.put('http://todos.api.netlor.fr/lists/'+tache.list_id["$oid"]+'/todos/'+tache.id, { text: tache.changeNameTache })
			.then(function(response){
					tache.text = tache.changeNameTache;
			});
		}else{
			alert('Impossible de modifier le nom de la tâche !! ');
		}
		
	}


		

}]);