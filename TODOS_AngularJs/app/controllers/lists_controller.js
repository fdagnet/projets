angular.module('tache').controller('ListsController',
	['$scope','$http','$rootScope',

	function($scope, $http, $rootScope){
	
		$http.defaults.headers.common.Authorization='3b34bc7fd41c4b5e93a6174df4b4028b';
		$http.get('http://todos.api.netlor.fr/lists')
		.then(function(response){
			$scope.lists =[];
			response.data.forEach(function(data){
				$scope.lists.push(data);

			});
		});

		$scope.reload=function(){
			$http.get('http://todos.api.netlor.fr/lists')
			.then(function(response){
				$scope.lists =[];
				response.data.forEach(function(data){
					$scope.lists.push(data);

				});
			});
		}

		$scope.add=function(){
			if(!$scope.nameList==''){
				$http.post('http://todos.api.netlor.fr/lists', { label: $scope.nameList })
				.then(function(response){
					$scope.reload();
				},function(error){

				});
			}else{
				alert('Impossible d\'ajouter une liste sans nom !! ');
			}
		},

		$scope.delete=function(a){
			
			$http.delete('http://todos.api.netlor.fr/lists/'+a)
			.then(function(response){
				$scope.reload();
			},function(error){

			});
		},

		$scope.update=function(list){
			
			if(!list.changeNameList==''){
				$http.put('http://todos.api.netlor.fr/lists/'+list.id, { label: list.changeNameList })
				.then(function(response){
					$scope.reload();
				},function(error){

				});
			}else{
				alert('Impossible de modifier le nom de la liste !! ');
			}
		},

		$scope.afficher=function(a){
			
			
				$http.get('http://todos.api.netlor.fr/lists/'+a+'/todos')
				.then(function(response){
					$rootScope.listSelect=a;
					$rootScope.selectedlist=a;
					$rootScope.listetaches =[];
					response.data.forEach(function(data){
					$rootScope.listetaches.push(data);
					console.log($rootScope.listetaches);
					console.log($rootScope.selectedlist);
					console.log($rootScope.listSelect);


					

				});
			});
			
		
		}

	}

]);