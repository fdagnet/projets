angular.module('tache').service('Tache', ['$http',
	function($http){
		var Tache= function(data){
			this.name = data.name;
			this.priority = data.priority;
			this.done = data.done;
		
		}
		return Tache;
}]);