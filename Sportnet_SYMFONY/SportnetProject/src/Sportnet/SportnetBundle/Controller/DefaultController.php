<?php

namespace Sportnet\SportnetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use models;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SportnetSportnetBundle:Default:index.html.twig', array('name' => $name));
    }

    public function accueilAction()
    {
    	$session = new Session();
    	if($session->get('id') == '')
    	{
    		$session->start();
    	}
    	if ($session->get('connecte') == true)
    	{
    		return $this->redirectToRoute('sportnet_sportnet_accueil_organisateur');
    	}
    	else{
    		return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
    	}
    }

    public function accueil_organisateurAction()
    {
        $session = new Session();
        if ($session->get('connecte') == false){
            return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
        }
        else
        {
            $user = $session->get('mail');
            $pdo = models\PdoSportnet::getPdoSportnet();
            $lesEvents = $pdo->user_allevents($user);
            $session->set('id',$user);
            return $this->render('SportnetSportnetBundle:Default:accueil_organisateur.html.twig', array('lesEvents' => $lesEvents));
        }
    }

    public function classement_etapeAction(Request $request)
    {
        $session = new Session();

    	$pdo = models\PdoSportnet::getPdoSportnet();
        $request->request->has('detailetape');
        $idTrial = $request->get('id');

        $name_trial = $pdo->name_trial($idTrial);
        
        $session->set('nametrial', $name_trial);

        $res = $pdo->getRankTrial($idTrial);

        return $this->render('SportnetSportnetBundle:Default:classement_etape.html.twig',array('leTableau'=> $res, 'Name_event' => $session->get('nameevent'), 'Name_trial' => $name_trial));
    }

    public function confirmation_inscriptionsAction(Request $request)
    {
        $session = new Session();
        $request->request->has('subscribe');
        
        $pdo = models\PdoSportnet::getPdoSportnet();
        $res = $pdo->getPricetrial($session->get('idtrial'));

        $name= $request->get('Name');
        $session->set('name',$name);

        $firstname= $request->get('Firstname');
        $session->set('firstname',$firstname);

        $mail= $request->get('Mail');
        $session->set('mail_part',$mail);

        $age= $request->get('Age');
        $session->set('age',$age);

        $paiement= $request->get('paiement');
        $num_card= $request->get('num_card');

        if (($name == ('')) || ($firstname== ('')) || ($mail == ('')) || ($age == (''))) 
        {
            $this->get('session')->getFlashBag()->add('notice', 'Veuillez remplir tous les champs obligatoires');
            return $this->redirectToRoute('sportnet_sportnet_event');
        }
        else
        {
            return $this->render('SportnetSportnetBundle:Default:confirmation_inscription.html.twig', array('name' => $name, 'firstname' => $firstname, 'mail' => $mail, 'age' => $age, 'paiement' => $paiement, 'price' => $res, 'num_card' => $num_card, 'Name_event' =>  $session->get('nameevent'), 'Name_trial' => $session->get('nametrial')));
        }
    	
    }

    public function connexionformAction()
    {
    	return $this->render('SportnetSportnetBundle:Default:connexion.html.twig');
    }

    public function connexionAction(Request $request)
    {
    	$session = new Session();
    	if($session->get('mail') == '')
    	{
    		$session->start();
    	}
    	$session->set('connecte',false);
    	
    	
    	if ($request->request->has('Connexion'))
    	{
    		$mail = $request->get('mail');
    		$mdp = $request->get('psw');
    		
    		$pdo = models\PdoSportnet::getPdoSportnet();
    		$res = $pdo->getInfosOrganizers($mail,$mdp);
            
    		if (count($res) == 0)
    		{
                $this->get('session')->getFlashBag()->add('notice', 'Identifiants non valides.');
    			return $this->render('SportnetSportnetBundle:Default:connexion.html.twig');
    		}
    		else
    		{
                $session->set('mail',$mail);
    			$session->set('connecte', true);
    			$session->set('iduser', $mail);
    			return $this->redirectToRoute('sportnet_sportnet_accueil_organisateur');
    		}
    	}
        return $this->redirectToRoute('sportnet_sportnet_accueil');
    }

	public function deconnexionAction(Request $request)
    {
    	$session = new Session();
    	if ($session->get('mail') == '')
    	{
    		$session->start();

    	}
    	if ($session->get('connecte') == true)
    	{
    		$session->clear('mail');
    		$ok = false;
    		return $this->redirectToRoute('sportnet_sportnet_accueil');
    	}
    	else
    	{
    		return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
    	}
    }

    public function createOrganizerAction(Request $request)
    {

    	if($request->request->has('valider'))
    	{
    		$mail = $request->get('mail');
    		$mdp = $request->get('psw');
    		$confirm_mdp = $request->get('confirm_psw');
    		$name = $request->get('name');
    		$firstname = $request->get('firstname'); 
    		if ($mdp != $confirm_mdp)
    		{
    			$this->get('session')->getFlashBag()->add('notice', 'Les mots de passes ne sont pas identiques');
    			return $this->render('SportnetSportnetBundle:Default:creer_compte.html.twig');
    		}
    		else if (($mail == ('')) || ($mdp == ('')) || ($confirm_mdp == ('')) || ($name == ('')) || ($firstname == ('')))
    		{
    			$this->get('session')->getFlashBag()->add('notice', 'Veuillez remplir tous les champs');
    			return $this->render('SportnetSportnetBundle:Default:creer_compte.html.twig');
    		}
    		else
    		{
    			$pdo = models\PdoSportnet::getPdoSportnet();
    			$pdo->createOrganizer($name, $firstname, $mail, $mdp);
    			
    		}
    	}
    	return $this->render('SportnetSportnetBundle:Default:connexion.html.twig');
    }

    public function creer_compteAction()
    {
    	return $this->render('SportnetSportnetBundle:Default:creer_compte.html.twig');
    }

    public function creer_eventAction(Request $request)
    {
        $session = new Session();
        if ($session->get('connecte') == false){
            return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
        }
        else
        {
            if($request->request->has('creerEvent'))
            {
                $name = $request->get('nom');
                $date_begin = $request->get('datedebut');
                $date_end = $request->get('datefin');
                $description = $request->get('description');
                $image = $request->get('image');
                $user = $session->get('mail');
                
                if (($name == ('')) || ($date_begin == ('')) || ($date_end == ('')) || ($description == ('')))
                {
                    $this->get('session')->getFlashBag()->add('notice', 'Veuillez remplir tous les champs');
                    return $this->render('SportnetSportnetBundle:Default:creer_event.html.twig');
                }
                else
                {
                    $pdo = models\PdoSportnet::getPdoSportnet();
                    $pdo->insert_event($name,$date_begin,$date_end,$description,$image,$user);
                }
            }
             return $this->redirectToRoute('sportnet_sportnet_accueil');
        }
    }

    public function modif_eventAction(Request $request)
    {
    	$session = new Session();
        if ($session->get('connecte') == false){
            return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
        }
        else
        {
        return $this->render('SportnetSportnetBundle:Default:modif_event.html.twig');
        }
    }

    public function etapeAction(Request $request)
    {
        $session = new Session();
        $pdo = models\PdoSportnet::getPdoSportnet();
        $request->request->has('detailevent');
        $id = $request->get('id');
        $session->set('id_event', $id); 
    	$res = $pdo->Trialfutur($id);

        $name_event=$pdo->name_event($session->get('id_event'));
        $session->set('nameevent', $name_event);

    	return $this->render('SportnetSportnetBundle:Default:etape.html.twig', array('lesEtapes' => $res, 'Name_event' => $name_event));
    }

    public function eventAction()
    {

    	$pdo = models\PdoSportnet::getPdoSportnet();
    	$res = $pdo->eventsfutur();
        
    	return $this->render('SportnetSportnetBundle:Default:event.html.twig', array('lesEvents' => $res));
    }

    public function inscriptions_epreuveAction(Request $request)
    {

        $session = new Session();

        $pdo = models\PdoSportnet::getPdoSportnet();
        $request->request->has('detailetape');
        $idTrial = $request->get('id');

        $session->set('idtrial', $idTrial);

        $name_trial = $pdo->name_trial($idTrial);
        $session->set('nametrial', $name_trial);

        $res = $pdo ->getInfo_trial($idTrial);

              

    	return $this->render('SportnetSportnetBundle:Default:inscriptions_epreuve.html.twig', array('Infos' => $res, 'Name_event' =>  $session->get('nameevent'), 'Name_trial' => $name_trial));
    }

    public function past_etapeAction(Request $request)
    {
        $session = new Session();
        $pdo = models\PdoSportnet::getPdoSportnet();
        $request->request->has('detailevent');
        $id = $request->get('id');
        $session->set('idevent', $id); 
        $res = $pdo->Trialfini($id);

        $name_event=$pdo->name_event($session->get('idevent'));
        $session->set('nameevent', $name_event);

        return $this->render('SportnetSportnetBundle:Default:past_etape.html.twig', array('leTableau'=> $res,  'Name_event' => $name_event));

    }

    public function past_eventAction()
    {
        $pdo = models\PdoSportnet::getPdoSportnet();
        $res = $pdo->eventsfini();
       
        return $this->render('SportnetSportnetBundle:Default:past_event.html.twig', array('leTableau'=> $res));
    }

    public function mes_inscriptionsAction(Request $request)
    {
        $session = new Session();

        if($request->request->has('inscription'))
        {
            $session->set('inscription',true);
            $pdo = models\PdoSportnet::getPdoSportnet();
            $res = $pdo->getInscription($request->get('id_participant'));
            return $this->render('SportnetSportnetBundle:Default:mes_inscriptions.html.twig', array('leTableau' => $res));

        }
        return $this->render('SportnetSportnetBundle:Default:mes_inscriptions.html.twig');
    }

    public function mon_classementAction(Request $request)
    {
        $session = new Session();

        if($request->request->has('classement'))
        {
            $session->set('classement',true);
            $pdo = models\PdoSportnet::getPdoSportnet();
            $res = $pdo->getRank($request->get('id_participant'));
            return $this->render('SportnetSportnetBundle:Default:mon_classement.html.twig', array('leTableau' => $res));
        }
        return $this->render('SportnetSportnetBundle:Default:mon_classement.html.twig');
    }

    public function gerer_trialAction(Request $request)
    {
        $session = new Session();
        if ($session->get('connecte') == false)
        {
            return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
        }
        else
        {  
            $pdo = models\PdoSportnet::getPdoSportnet();
            if ( $request->request->has('affichtrial'))
            {
                $event=$request->get('id');
                $session->set('id_event', $event);
                $lesTrials = $pdo-> user_alltrial($event);
                return $this->render('SportnetSportnetBundle:Default:gerer_trial.html.twig', array('lesTrials' => $lesTrials));
            }
            
            else if ($request->request->has('modifevent'))
            {
                $name = $request->get('nameEvent');
                $date_begin = $request->get('dateBeginEvent');
                $date_end = $request->get('dateEndEvent');
                $description = $request->get('descriptionEvent');
                $image = $request->get('imageEvent');
                $event=$request->get('id');

                if (($name == ('')) || ($date_begin == ('')) || ($date_end == ('')) || ($description == ('')))
                {
                    $this->get('session')->getFlashBag()->add('notice', 'Veuillez remplir les champs obligatoires');
                    return $this->redirectToRoute('sportnet_sportnet_accueil_organisateur');
                }
                else
                {
                    $pdo->update_event($event,$name,$date_begin,$date_end,$description,$image);
                    return $this->redirectToRoute('sportnet_sportnet_accueil_organisateur');
                }           
            
            } 
        } 
    }

    public function modif_trialAction(Request $request)
    {   
        $session = new Session();
        if ($session->get('connecte') == false)
        {
            return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
        }
        else
        {
            $pdo = models\PdoSportnet::getPdoSportnet();
            if($request->request->has('modiftrial'))
            {
                $id = $request->get('idTrial');
                $name = $request->get('nameTrial');
                $date_begin = $request->get('dateBeginTrial');
                $date_end = $request->get('dateEndTrial');
                $locality = $request->get('localityTrial');
                $max_paticipants = $request->get('maxParticipantsTrial');
                $price=$request->get('priceTrial');
                $age_min = $request->get('ageMinTrial');  
                $age_max = $request->get('ageMaxTrial');
                $image = $request->get('imageTrial');
                $close = $request->get('closeTrial');

                if (($name == ('')) || ($date_begin == ('')) || ($date_end == ('')) || ($locality == ('')) || ($max_paticipants == ('')) || ($price == ('')) || ($age_min == ('')) || ($age_max == ('')) || ($close == ('')))
                {
                    $this->get('session')->getFlashBag()->add('notice', 'Veuillez remplir les champs obligatoires');
                    return $this->redirectToRoute('sportnet_sportnet_accueil');
                }
                else
                {
                    $pdo->update_trial($id,$name, $date_begin, $date_end, $locality, $max_paticipants, $price, $age_min, $age_max, $image, $close);
                    return $this->redirectToRoute('sportnet_sportnet_accueil');
                    
                }
            }
        }
    }

     public function inscrireAction()
    {
        $session = new Session();
        $pdo = models\PdoSportnet::getPdoSportnet();
        $pdo->insert_participant($session->get('name'),$session->get('firstname'),$session->get('mail_part'),$session->get('age'));
        $num_participant = $pdo->Participant_number($session->get('mail_part'));
        $pdo->insertRank($num_participant,$session->get('idtrial'));
        $mes='Votre numéro de dossart est '.$num_participant." Il est necessaire de le noter ou de le retenir.";
        $this->get('session')->getFlashBag()->add('notice', $mes);
        return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
    }

    public function formcreate_trialAction()
    {
         $session = new Session();
        if ($session->get('connecte') == false){
            return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
        }
        else
        {
            $pdo = models\PdoSportnet::getPdoSportnet();
            $lesSports = $pdo->getSport();
            return $this->render('SportnetSportnetBundle:Default:creer_trial.html.twig', array('lesSports' => $lesSports));
        }
    }

    public function creer_trialAction(Request $request)
    {
        $session = new Session();
        if ($session->get('connecte') == false){
            return $this->render('SportnetSportnetBundle:Default:accueil.html.twig');
        }
        else
        {
            if($request->request->has('creerTrial'))
            {
                $name = $request->get('nameTri');
                $date_begin = $request->get('dateBeginTri');
                $date_end = $request->get('dateEndTri');
                $locality = $request->get('localityTri');
                $max_participants = $request->get('maxParticipantsTri');
                $price = $request->get('priceTri');
                $age_min = $request->get('ageMinTri');
                $age_max = $request->get('ageMaxTri');
                $image = $request->get('imageTri');
                $close = $request->get('close');
                $id_event = $session->get('id_event');
                $id_sport = $request->get('sport');
                
                if (($name == ('')) || ($date_begin == ('')) || ($date_end == ('')) || ($locality == ('')) || ($max_participants == ('')) || ($price == ('')) || ($age_min == ('')) || ($age_max == ('')) || ($close == ('')) || ($id_event == ('')) || ($id_sport == ('')))
                {
                    $this->get('session')->getFlashBag()->add('notice', 'Veuillez remplir tous les champs');
                    return $this->redirectToRoute('sportnet_sportnet_formcreate_trial');
                }
                else
                {
                    $pdo = models\PdoSportnet::getPdoSportnet();
                    $pdo->insert_trial($name, $date_begin, $date_end, $locality, $max_participants, $price, $age_min, $age_max, $image, $close, $id_event, $id_sport);
                    return $this->redirectToRoute('sportnet_sportnet_accueil_organisateur');
                }
            }
        }
    }

    public function formcreer_eventAction()
    {
        $session = new Session();
        if ($session->get('connecte') == false){
            return $this->render('SportnetSportnetBundle:Default:connexion.html.twig');
        }
        else
        {
            return $this->render('SportnetSportnetBundle:Default:creer_event.html.twig');
        }
    }
}
