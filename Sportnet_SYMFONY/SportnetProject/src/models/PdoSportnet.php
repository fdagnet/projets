<?php

namespace models;

class PdoSportnet {
	private static $serveur = 'mysql:host=127.0.0.1';
	private static $bdd = 'dbname=dagnet2u';
	private static $user = 'dagnet2u';
	private static $mdp = 'Jihava&5';
	private static $monPdo;
	private static $monPdoSportnet = null;

	public function __construct()
	{
		PdoSportnet::$monPdo = new \PDO(PdoSportnet::$serveur.';'.PdoSportnet::$bdd, PdoSportnet::$user, PdoSportnet::$mdp);
		PdoSportnet::$monPdo->query("SET CHARACTER SET utf8");
	}

	public function __destruct()
	{
		PdoSportnet::$monPdo = null;
	}

	public static function getPdoSportnet()
	{
		if(PdoSportnet::$monPdoSportnet == null)
		{
			PdoSportnet::$monPdoSportnet = new PdoSportnet();
		}
		return PdoSportnet::$monPdoSportnet;
	}

	// REQUETE SUR TABLE EVENT

	public function eventsfutur()
	{
		$date = date("Y-m-d");
		$req = "SELECT Id, Name, Date_begin, Image FROM event WHERE Date_end > '$date'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function eventsfini()
	{
		$date = date("Y-m-d");
		$req = "SELECT Id, Name, Date_begin, Image FROM event WHERE Date_end < '$date'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;		
	}

	public function user_allevents($user)
	{
		$req = "SELECT Id, Name, Description, Date_begin , Date_end, Image FROM event where Mail_organizer = '$user'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function user_eventsfutur($user)
	{
		$date = date("Y-m-d");
		$req = "SELECT Name, Description, Date_begin , Date_end FROM event where Mail_organizer = '$user' AND Date_end > '$date'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function user_eventsfini($user)
	{
		$date = date("Y-m-d");
		$req = "SELECT Name, Description, Date_begin, Date_end FROM event where Mail_organizer = '$user' AND Date_end < '$date'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function insert_event($name,$date_begin,$date_end,$description,$image,$user)
	{
		$req = "INSERT INTO event VALUES ('','$name','$date_begin','$date_end','$description','$image','$user')";
		PdoSportnet::$monPdo->exec($req);
	}

	public function update_event($id,$name,$date_begin,$date_end,$description,$image)
	{
		$req = "UPDATE event SET Name = '$name', Date_begin = '$date_begin', Date_end = '$date_end', Description = '$description', Image = '$image' WHERE Id = '$id'";
		PdoSportnet::$monPdo->exec($req);
	}

	public function name_event($id)
	{
		$req = "SELECT Name FROM event WHERE Id='$id'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes[0]['Name'];
	}

	public function descr_event($id)
	{
		$req = "SELECT Description FROM event WHERE Id='$id'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		
		return $lesLignes[0]['Description'];
	}

	// REQUETE SUR TABLE ORGANIZERS

	public function getInfosOrganizers($mail,$mdp)
	{
		$req = "SELECT * FROM organizers WHERE E_mail = '$mail' AND Password = '$mdp'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

		
	public function createOrganizer($name, $firstname, $mail, $password)
	{
		$req = "INSERT INTO organizers (Name, First_name, E_mail, Password) VALUES ('$name', '$firstname', '$mail', '$password')";
		PdoSportnet::$monPdo->exec($req);
	}

	// REQUETE SUR TABLE PARTICIPANTS

	public function allparticipants()
	{
		$req = "SELECT * FROM participants ";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function insert_participant($name,$firstname,$mail,$age)
	{
		$req = "INSERT INTO participants (Name,Firstname,E_mail,Age) VALUES ('$name','$firstname','$mail','$age')";
		PdoSportnet::$monPdo->exec($req);
	}

	public function Participant_number($mail)
	{
		$req = "SELECT Participant_number FROM participants where E_mail='$mail'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes[0]['Participant_number'];
	}

	//REQUETE SUR TABLE RANKING

	public function getRankTrial($idtrial)
	{
		$req = "SELECT Rank, Time_trial, Participant_number FROM ranking INNER JOIN trial ON ranking.Id=trial.Id WHERE trial.Id = '$idtrial' order by Rank";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}


	public function getRankParticipant($idparticipant, $idtrial)
	{
		$req = "SELECT Rank as 'Rang', trial.Name as 'Nom de l etape', trial.Locality as 'Lieu', sport.Name as 'Sport' FROM sport INNER JOIN trial ON sport.Id=trial.Id_Sport INNER JOIN ranking ON ranking.Id=trial.Id WHERE Participant_number = '$idparticipant' AND trial.Id = $idtrial";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function getRank($idparticipant)
	{
		$req = " SELECT Rank, Time_trial, trial.Name as Name FROM ranking INNER JOIN trial ON ranking.Id=trial.Id WHERE ranking.Participant_number='$idparticipant'";
		$res =PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function insertRank($idparticipant, $idtrial) 
	{
		$req = "INSERT INTO ranking (Participant_number,Id)VALUES ('$idparticipant','$idtrial')";
		PdoSportnet::$monPdo->exec($req);			
	}

	// REQUETE SUR TABLE TRIAL

	public function Trialfutur($event){
		$date = date("Y-m-d");
		$req = "SELECT Id, Name, Date_begin, Image FROM trial WHERE Id_Event = '$event' AND Date_end > '$date'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function Trialfini($event){
		$date = date("Y-m-d");
		$req = "SELECT Id, Name, Date_begin, Image FROM trial WHERE Id_Event = '$event' AND Date_end < '$date'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function user_alltrial($event){
		$req = "SELECT Id, Name, Date_begin, Date_end, Locality, Max_participants, Price, Age_min, Age_max, Image, Close FROM trial WHERE Id_Event = '$event'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function user_trialfutur($event){
		$req = "SELECT * FROM trial WHERE Id_Event = '$event' AND Date_end > 'PdoSportnet::$datenow'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function user_trialfini(){
		$req = "SELECT * FROM trial WHERE Id_Event = '$event' AND Date_end < '".PdoSportnet::$datenow."'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function insert_trial($name, $date_begin, $date_end, $locality, $max_participants, $price, $age_min, $age_max, $image, $close, $id_event, $id_sport)
	{
		$req = "INSERT INTO trial values ('', '$name','$date_begin','$date_end','$locality','$max_participants','$price','$age_min','$age_max','$image','$close','$id_event','$id_sport')";
		PdoSportnet::$monPdo->exec($req);
	}

	public function update_trial($id,$name, $date_begin, $date_end, $locality, $max_participants, $price, $age_min, $age_max, $image, $close)
	{
		$req = "UPDATE trial SET Name = '$name', Date_begin = '$date_begin', Date_end = '$date_end', Locality = '$locality', Max_participants = '$max_participants', Price = '$price', Age_min = '$age_min', Age_max = '$age_max', Image = '$image', Close = '$close' WHERE Id = '$id'";
		PdoSportnet::$monPdo->exec($req);
	}

	public function getPricetrial($trial)
	{
		$req = "SELECT Price FROM trial WHERE Id = '$trial'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes[0]['Price'];
	}

	public function getInscription($idparticipant)
	{
		$req = " SELECT trial.Name, trial.Date_begin, trial.Date_end, trial.Locality, trial.Close, sport.Name as Sport FROM trial INNER JOIN sport ON trial.Id_Sport=sport.Id INNER JOIN ranking ON trial.Id=ranking.Id WHERE ranking.Participant_number='$idparticipant'";
		$res =PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function name_trial($id)
	{
		$req = "SELECT Name FROM trial WHERE Id='$id'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes[0]['Name'];
	}

	public function getInfo_trial($id)
	{
		$req = "SELECT * FROM trial WHERE Id='$id'";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	// REQUETE TABLE SPORT

	public function getSport()
	{
		$req = "SELECT * FROM sport";
		$res = PdoSportnet::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}
}