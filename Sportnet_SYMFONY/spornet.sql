-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 01, 2017 at 10:57 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.14-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spornet`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date_begin` date NOT NULL,
  `date_end` date NOT NULL,
  `description` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `id_organizers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `name`, `date_begin`, `date_end`, `description`, `image`, `id_organizers`) VALUES
(1, 'CourseapiedEvent', '2016-11-23', '2016-11-23', 'course a pied pour tout âges', '', 1),
(2, 'vtt 1', '2016-11-13', '2016-11-14', 'Tournoi vtt numéro 1', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `organizers`
--

CREATE TABLE `organizers` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `e_mail` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizers`
--

INSERT INTO `organizers` (`id`, `name`, `first_name`, `e_mail`, `password`) VALUES
(1, 'Glace', 'Brice', 'brice.glace@gmail.com', 'glace55'),
(2, 'Daisy', 'Rable', 'daisy.rable@outlook.fr', 'daisy56');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `participant_number` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `e_mail` varchar(250) NOT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`participant_number`, `name`, `firstname`, `e_mail`, `age`) VALUES
(1, 'Etonsac', 'David', 'david.etonsac@gmail.com', 12),
(2, 'Tefairevoir', 'Eva', 'eva@gmail.com', 17),
(3, 'Tation', 'Félicie', 'Tation@gmail.com', 16),
(4, 'De Michelin', 'Guy', 'Guy@gmail.com', 45),
(5, 'Cobeure', 'Harry', 'Harry@gmail.com', 35),
(6, 'Ouzi', 'Jacques', 'Ouzi@gmail.com', 20),
(7, 'Deuf', 'John', 'Deuf@gmail.com', 7),
(8, 'Rines', 'Léna', 'lena@gmail.com', 10),
(9, 'Rouana', 'Marie', 'marie@gmail.com', 15),
(10, 'Ateur', 'Nordine', 'ateur@gmail.com', 19),
(11, 'Emploi', 'Paul', 'paul@gmail.com', 25),
(12, 'Croche', 'Sarah', 'sarah@gmail.com', 30),
(13, 'Egérie', 'Tom', 'tom@gmail.com', 35),
(14, 'Time', 'Vincent', 'time@gmail.com', 45),
(15, 'Kafairgaf', 'Xavier', 'xavier@gmail.com', 54);

-- --------------------------------------------------------

--
-- Table structure for table `ranking`
--

CREATE TABLE `ranking` (
  `rank` int(11) DEFAULT NULL,
  `time_trial` time DEFAULT NULL,
  `cover_number` int(25) NOT NULL,
  `participant_number` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ranking`
--

INSERT INTO `ranking` (`rank`, `time_trial`, `cover_number`, `participant_number`, `id`) VALUES
(NULL, NULL, 1, 1, 1),
(NULL, NULL, 2, 2, 1),
(NULL, NULL, 3, 3, 1),
(NULL, NULL, 1, 4, 2),
(NULL, NULL, 2, 5, 2),
(NULL, NULL, 3, 6, 2),
(2, '02:00:00', 1, 7, 3),
(1, '01:30:00', 2, 8, 3),
(3, '03:00:00', 3, 9, 3),
(1, '01:00:00', 1, 10, 4),
(3, '02:00:00', 2, 11, 4),
(2, '01:30:00', 3, 12, 4),
(3, '03:00:00', 1, 13, 5),
(2, '01:35:00', 2, 14, 5),
(1, '01:00:00', 3, 15, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sport`
--

CREATE TABLE `sport` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sport`
--

INSERT INTO `sport` (`id`, `name`) VALUES
(1, 'Course à pied'),
(2, 'Cyclisme'),
(3, 'VTT'),
(4, 'Triathlon'),
(7, 'Marathon'),
(9, 'Natation');

-- --------------------------------------------------------

--
-- Table structure for table `trial`
--

CREATE TABLE `trial` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `date_begin` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `locality` varchar(100) NOT NULL,
  `max_participants` int(11) NOT NULL,
  `price` float NOT NULL,
  `age_min` int(11) NOT NULL,
  `age_max` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `close` set('Clôt','Ouvert') NOT NULL,
  `id_Event` int(11) NOT NULL,
  `id_Sport` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trial`
--

INSERT INTO `trial` (`id`, `name`, `date_begin`, `date_end`, `locality`, `max_participants`, `price`, `age_min`, `age_max`, `image`, `close`, `id_Event`, `id_Sport`) VALUES
(1, 'COURSE A PIED enfant', '2016-11-23 10:00:00', '2016-11-23 12:30:00', 'Behonne', 20, 3.5, 10, 17, '', 'Ouvert', 1, 1),
(2, 'course a pied adulte', '2016-11-23 15:00:00', '2016-11-23 17:30:00', 'Behonne', 25, 5, 18, 50, '', 'Ouvert', 1, 1),
(3, 'vtt enfant', '2016-11-13 09:00:00', '2016-11-13 13:00:00', 'Ligny', 10, 2.5, 7, 16, '', 'Clôt', 2, 3),
(4, 'vtt daulte', '2016-11-13 15:00:00', '2016-11-13 19:00:00', 'Ligny', 20, 3.5, 18, 30, '', 'Clôt', 2, 3),
(5, 'vtt senior', '2016-11-14 14:00:00', '2016-11-14 17:00:00', 'Ligny', 20, 3.5, 35, 55, '', 'Clôt', 2, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Event_Id_Organizers` (`id_organizers`);

--
-- Indexes for table `organizers`
--
ALTER TABLE `organizers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`participant_number`);

--
-- Indexes for table `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`participant_number`,`id`),
  ADD KEY `FK_Ranking_Id` (`id`),
  ADD KEY `Participant_number` (`participant_number`);

--
-- Indexes for table `sport`
--
ALTER TABLE `sport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trial`
--
ALTER TABLE `trial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Trial_Id_Event` (`id_Event`),
  ADD KEY `FK_Trial_Id_Sport` (`id_Sport`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `organizers`
--
ALTER TABLE `organizers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `participant_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sport`
--
ALTER TABLE `sport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `trial`
--
ALTER TABLE `trial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FK_Event_Id_Organizers` FOREIGN KEY (`id_organizers`) REFERENCES `organizers` (`id`);

--
-- Constraints for table `ranking`
--
ALTER TABLE `ranking`
  ADD CONSTRAINT `FK_Ranking_Id` FOREIGN KEY (`id`) REFERENCES `trial` (`id`),
  ADD CONSTRAINT `FK_Ranking_Participant_number` FOREIGN KEY (`participant_number`) REFERENCES `participants` (`participant_number`);

--
-- Constraints for table `trial`
--
ALTER TABLE `trial`
  ADD CONSTRAINT `FK_Trial_Id_Event` FOREIGN KEY (`id_Event`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_Trial_Id_Sport` FOREIGN KEY (`id_Sport`) REFERENCES `sport` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
