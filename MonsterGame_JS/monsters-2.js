var monster = {
    modules : {}
};


monster.modules.actions = (function(){
    var name = null;
    var life = null;
    var money = null;
    var awake = true;


    return{

        showme : function(){
            monster.modules.app.log(">> Name : "+name+"\n | \nLife : "+life+"\n | \nMoney : "+money);
            monster.modules.app.displayStatus(life, money, awake);
        },


        init : function(name_monster, life_monster, money_monster, awake_monster){
            name = name_monster;
            life = life_monster;
            money = money_monster;
            awake = awake_monster;
        },


        estVivant : function(){
            if(life > 0)
                return true;
            else
                return false;
        },


        run : function(){
            if(awake && monster.modules.actions.estVivant()){
                monster.modules.app.log(name+" lost 1 life point.");
                life--;
                if(life < 0){
                    life = 0;
                }
                monster.modules.actions.showme();
            }
        },


        fight : function(){
            if(awake && monster.modules.actions.estVivant()){
                monster.modules.app.log(name+" lost 3 life points.");
                life-=3;
                if(life < 0){
                    life = 0;
                }
                monster.modules.actions.showme();
            }
        },


        work : function(){
            if(awake && monster.modules.actions.estVivant()){
                monster.modules.app.log(name+" lost 1 life point and won 2 coins.");
                life--;
                if(life < 0){
                    life = 0;
                }
                money++;
                money++;
                monster.modules.actions.showme();
            }
        },


        eat : function(){
            if(awake && monster.modules.actions.estVivant()){
                if(money >= 3){
                    monster.modules.app.log(name+" won 2 life points and lost 3 coins.");
                    money-=3;
                    life++;
                    life++;
                    monster.modules.actions.showme();
                }else{
                    monster.modules.app.log(name+" doesn't have enough coins to eat.");
                }
            }
        },


        sleep : function(){
            if(awake && monster.modules.actions.estVivant()) {
                monster.modules.app.log(name + " fall asleep...");
                awake = false;
                setTimeout(function () {
                    life++;
                    monster.modules.app.log(name + " has woken up and won 1 life point.");
                    awake = true;
                    monster.modules.actions.showme();
                }, 10000);
            }
        },


        kill : function(){
            if(monster.modules.actions.estVivant()){
                life = 0;
                monster.modules.app.log(name + " dies.");
            }else
                monster.modules.app.log(name + " is already dead.");
        },


        newlife : function(){
            monster.modules.app.run(name_monster, life_monster, money_monster, true);
            //monster.modules.app.displayStatus(monster.modules.actions.life, monster.modules.actions.money, monster.modules.actions.awake);
            monster.modules.app.log(name + " come back to life.")
        },

        life : function(){
            life--;
            monster.modules.app.log(name+" lost 1 life point cause SetInterval.");
        },
    }

})();



monster.modules.app = (function(){
    var run;
    var fight;
    var work;
    var sleep;
    var eat;
    var show;
    var life;

    return{


        run : function(name_monster,life_monster,money_monster,awake_monster){
            monster.modules.actions.init(name_monster,life_monster,money_monster,awake_monster);
            document.getElementById("b1").onclick=monster.modules.actions.newlife;
            document.getElementById("b2").onclick=monster.modules.actions.run;
            document.getElementById("b3").onclick=monster.modules.actions.fight;
            document.getElementById("b4").onclick=monster.modules.actions.sleep;
            document.getElementById("b5").onclick=monster.modules.actions.eat;
            document.getElementById("b6").onclick=monster.modules.actions.showme;
            document.getElementById("b7").onclick=monster.modules.actions.work;
            document.getElementById("k").onclick=monster.modules.actions.kill;
            monster.modules.actions.showme();


            window.setInterval(function(){
                    if(monster.modules.actions.estVivant()){
                        monster.modules.actions.life();
                        // monster.modules.action.life_monster--;
                        // monster.modules.app.log(monster.modules.action.name + " lost 1 life point.");
                        var act = parseInt(Math.random()*5);
                        switch(act){
                            case 0:
                                monster.modules.actions.run();
                                break;
                            case 1:
                                monster.modules.actions.fight();
                                break;
                            case 2:
                                monster.modules.actions.work();
                                break;
                            case 3:
                                monster.modules.actions.sleep();
                                break;
                            case 4:
                                monster.modules.actions.eat();
                                break;
                        }
                    }
             }, 12000);
        },


        log : function(message){
            var p = document.createElement("p");
            p.appendChild(document.createTextNode(message));
            document.getElementById("actionbox").insertBefore(p, document.getElementById("actionbox").firstElementChild);
        },


        displayStatus : function(life_monster,money_monster,awake_monster){
            var li_life = document.createElement("li");
            var li_money = document.createElement("li");
            var li_awake = document.createElement("li");
            
            li_life.appendChild(document.createTextNode("life:"+life_monster+" "));
            li_money.appendChild(document.createTextNode("money:"+money_monster+" "));
            document.getElementById("status").replaceChild(li_life, document.getElementById("status").firstElementChild);
            document.getElementById("status").replaceChild(li_money, document.getElementById("status").firstElementChild.nextElementSibling);
            
            if(awake_monster)
                var awake = "awake";
            else
                var awake = "asleep";
            li_awake.appendChild(document.createTextNode(awake));
            document.getElementById("status").replaceChild(li_awake, document.getElementById("status").lastElementChild);

            var monster = document.getElementById("monster");
            if(life_monster >= 15)
                monster.style.backgroundColor="green";
            else if(life_monster >= 10)
                monster.style.backgroundColor="blue";
            else if(life_monster >= 5)
                monster.style.backgroundColor="orange";
            else if(life_monster>0)
                monster.style.backgroundColor="red";
            else
                monster.style.backgroundColor="gray";

            monster.style.borderWidth=money_monster*0.25+"px";
        }
    }
})();



var name_monster = prompt(" Monster's name : ");
var life_monster = prompt(" Monster's life : ");
var money_monster = prompt(" Monster's money : ");
window.onload=monster.modules.app.run(name_monster,life_monster,money_monster,true);



